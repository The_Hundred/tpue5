// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TP_UE5/TP_UE5GameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTP_UE5GameMode() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	TP_UE5_API UClass* Z_Construct_UClass_ATP_UE5GameMode();
	TP_UE5_API UClass* Z_Construct_UClass_ATP_UE5GameMode_NoRegister();
	UPackage* Z_Construct_UPackage__Script_TP_UE5();
// End Cross Module References
	void ATP_UE5GameMode::StaticRegisterNativesATP_UE5GameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATP_UE5GameMode);
	UClass* Z_Construct_UClass_ATP_UE5GameMode_NoRegister()
	{
		return ATP_UE5GameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATP_UE5GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATP_UE5GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TP_UE5,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATP_UE5GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "TP_UE5GameMode.h" },
		{ "ModuleRelativePath", "TP_UE5GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATP_UE5GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATP_UE5GameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATP_UE5GameMode_Statics::ClassParams = {
		&ATP_UE5GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATP_UE5GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATP_UE5GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATP_UE5GameMode()
	{
		if (!Z_Registration_Info_UClass_ATP_UE5GameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATP_UE5GameMode.OuterSingleton, Z_Construct_UClass_ATP_UE5GameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATP_UE5GameMode.OuterSingleton;
	}
	template<> TP_UE5_API UClass* StaticClass<ATP_UE5GameMode>()
	{
		return ATP_UE5GameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATP_UE5GameMode);
	ATP_UE5GameMode::~ATP_UE5GameMode() {}
	struct Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATP_UE5GameMode, ATP_UE5GameMode::StaticClass, TEXT("ATP_UE5GameMode"), &Z_Registration_Info_UClass_ATP_UE5GameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATP_UE5GameMode), 2970533901U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_2339634149(TEXT("/Script/TP_UE5"),
		Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
