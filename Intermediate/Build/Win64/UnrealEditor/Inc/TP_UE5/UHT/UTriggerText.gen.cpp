// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TP_UE5/UTriggerText.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUTriggerText() {}
// Cross Module References
	TP_UE5_API UClass* Z_Construct_UClass_UUTriggerText();
	TP_UE5_API UClass* Z_Construct_UClass_UUTriggerText_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_TP_UE5();
// End Cross Module References
	DEFINE_FUNCTION(UUTriggerText::execSetText)
	{
		P_GET_PROPERTY(FTextProperty,Z_Param_button);
		P_GET_PROPERTY(FTextProperty,Z_Param_verb);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetText(Z_Param_button,Z_Param_verb);
		P_NATIVE_END;
	}
	static FName NAME_UUTriggerText_UpdateWidgetText = FName(TEXT("UpdateWidgetText"));
	void UUTriggerText::UpdateWidgetText()
	{
		ProcessEvent(FindFunctionChecked(NAME_UUTriggerText_UpdateWidgetText),NULL);
	}
	void UUTriggerText::StaticRegisterNativesUUTriggerText()
	{
		UClass* Class = UUTriggerText::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetText", &UUTriggerText::execSetText },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUTriggerText_SetText_Statics
	{
		struct UTriggerText_eventSetText_Parms
		{
			FText button;
			FText verb;
		};
		static const UECodeGen_Private::FTextPropertyParams NewProp_button;
		static const UECodeGen_Private::FTextPropertyParams NewProp_verb;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUTriggerText_SetText_Statics::NewProp_button = { "button", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTriggerText_eventSetText_Parms, button), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUTriggerText_SetText_Statics::NewProp_verb = { "verb", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTriggerText_eventSetText_Parms, verb), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUTriggerText_SetText_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUTriggerText_SetText_Statics::NewProp_button,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUTriggerText_SetText_Statics::NewProp_verb,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUTriggerText_SetText_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UTriggerText.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UUTriggerText_SetText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUTriggerText, nullptr, "SetText", nullptr, nullptr, sizeof(Z_Construct_UFunction_UUTriggerText_SetText_Statics::UTriggerText_eventSetText_Parms), Z_Construct_UFunction_UUTriggerText_SetText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUTriggerText_SetText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUTriggerText_SetText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUTriggerText_SetText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUTriggerText_SetText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UUTriggerText_SetText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUTriggerText_UpdateWidgetText_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUTriggerText_UpdateWidgetText_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UTriggerText.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UUTriggerText_UpdateWidgetText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUTriggerText, nullptr, "UpdateWidgetText", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUTriggerText_UpdateWidgetText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUTriggerText_UpdateWidgetText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUTriggerText_UpdateWidgetText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UUTriggerText_UpdateWidgetText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UUTriggerText);
	UClass* Z_Construct_UClass_UUTriggerText_NoRegister()
	{
		return UUTriggerText::StaticClass();
	}
	struct Z_Construct_UClass_UUTriggerText_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_buttonText_MetaData[];
#endif
		static const UECodeGen_Private::FTextPropertyParams NewProp_buttonText;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_verbText_MetaData[];
#endif
		static const UECodeGen_Private::FTextPropertyParams NewProp_verbText;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUTriggerText_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_TP_UE5,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUTriggerText_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUTriggerText_SetText, "SetText" }, // 3540962730
		{ &Z_Construct_UFunction_UUTriggerText_UpdateWidgetText, "UpdateWidgetText" }, // 2381848395
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUTriggerText_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "UTriggerText.h" },
		{ "ModuleRelativePath", "UTriggerText.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUTriggerText_Statics::NewProp_buttonText_MetaData[] = {
		{ "Category", "UTriggerText" },
		{ "ModuleRelativePath", "UTriggerText.h" },
	};
#endif
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UClass_UUTriggerText_Statics::NewProp_buttonText = { "buttonText", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UUTriggerText, buttonText), METADATA_PARAMS(Z_Construct_UClass_UUTriggerText_Statics::NewProp_buttonText_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUTriggerText_Statics::NewProp_buttonText_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUTriggerText_Statics::NewProp_verbText_MetaData[] = {
		{ "Category", "UTriggerText" },
		{ "ModuleRelativePath", "UTriggerText.h" },
	};
#endif
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UClass_UUTriggerText_Statics::NewProp_verbText = { "verbText", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UUTriggerText, verbText), METADATA_PARAMS(Z_Construct_UClass_UUTriggerText_Statics::NewProp_verbText_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUTriggerText_Statics::NewProp_verbText_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUTriggerText_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUTriggerText_Statics::NewProp_buttonText,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUTriggerText_Statics::NewProp_verbText,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUTriggerText_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUTriggerText>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UUTriggerText_Statics::ClassParams = {
		&UUTriggerText::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UUTriggerText_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UUTriggerText_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUTriggerText_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUTriggerText_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUTriggerText()
	{
		if (!Z_Registration_Info_UClass_UUTriggerText.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UUTriggerText.OuterSingleton, Z_Construct_UClass_UUTriggerText_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UUTriggerText.OuterSingleton;
	}
	template<> TP_UE5_API UClass* StaticClass<UUTriggerText>()
	{
		return UUTriggerText::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUTriggerText);
	UUTriggerText::~UUTriggerText() {}
	struct Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UUTriggerText, UUTriggerText::StaticClass, TEXT("UUTriggerText"), &Z_Registration_Info_UClass_UUTriggerText, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UUTriggerText), 909417182U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_1679489948(TEXT("/Script/TP_UE5"),
		Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
