// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTP_UE5_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_TP_UE5;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_TP_UE5()
	{
		if (!Z_Registration_Info_UPackage__Script_TP_UE5.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/TP_UE5",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xD18544DF,
				0x3427424D,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_TP_UE5.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_TP_UE5.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_TP_UE5(Z_Construct_UPackage__Script_TP_UE5, TEXT("/Script/TP_UE5"), Z_Registration_Info_UPackage__Script_TP_UE5, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xD18544DF, 0x3427424D));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
