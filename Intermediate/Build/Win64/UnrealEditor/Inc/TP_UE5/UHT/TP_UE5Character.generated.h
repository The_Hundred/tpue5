// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "TP_UE5Character.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TP_UE5_TP_UE5Character_generated_h
#error "TP_UE5Character.generated.h already included, missing '#pragma once' in TP_UE5Character.h"
#endif
#define TP_UE5_TP_UE5Character_generated_h

#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_SPARSE_DATA
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_RPC_WRAPPERS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_ACCESSORS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATP_UE5Character(); \
	friend struct Z_Construct_UClass_ATP_UE5Character_Statics; \
public: \
	DECLARE_CLASS(ATP_UE5Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TP_UE5"), NO_API) \
	DECLARE_SERIALIZER(ATP_UE5Character)


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATP_UE5Character(); \
	friend struct Z_Construct_UClass_ATP_UE5Character_Statics; \
public: \
	DECLARE_CLASS(ATP_UE5Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TP_UE5"), NO_API) \
	DECLARE_SERIALIZER(ATP_UE5Character)


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATP_UE5Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATP_UE5Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATP_UE5Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_UE5Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATP_UE5Character(ATP_UE5Character&&); \
	NO_API ATP_UE5Character(const ATP_UE5Character&); \
public: \
	NO_API virtual ~ATP_UE5Character();


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATP_UE5Character(ATP_UE5Character&&); \
	NO_API ATP_UE5Character(const ATP_UE5Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATP_UE5Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_UE5Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATP_UE5Character) \
	NO_API virtual ~ATP_UE5Character();


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_11_PROLOG
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_SPARSE_DATA \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_RPC_WRAPPERS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_ACCESSORS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_INCLASS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_SPARSE_DATA \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_ACCESSORS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_INCLASS_NO_PURE_DECLS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TP_UE5_API UClass* StaticClass<class ATP_UE5Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
