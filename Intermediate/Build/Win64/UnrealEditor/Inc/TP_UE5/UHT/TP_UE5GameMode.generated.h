// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "TP_UE5GameMode.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TP_UE5_TP_UE5GameMode_generated_h
#error "TP_UE5GameMode.generated.h already included, missing '#pragma once' in TP_UE5GameMode.h"
#endif
#define TP_UE5_TP_UE5GameMode_generated_h

#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_SPARSE_DATA
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_RPC_WRAPPERS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_ACCESSORS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATP_UE5GameMode(); \
	friend struct Z_Construct_UClass_ATP_UE5GameMode_Statics; \
public: \
	DECLARE_CLASS(ATP_UE5GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TP_UE5"), TP_UE5_API) \
	DECLARE_SERIALIZER(ATP_UE5GameMode)


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATP_UE5GameMode(); \
	friend struct Z_Construct_UClass_ATP_UE5GameMode_Statics; \
public: \
	DECLARE_CLASS(ATP_UE5GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TP_UE5"), TP_UE5_API) \
	DECLARE_SERIALIZER(ATP_UE5GameMode)


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TP_UE5_API ATP_UE5GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATP_UE5GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TP_UE5_API, ATP_UE5GameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_UE5GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TP_UE5_API ATP_UE5GameMode(ATP_UE5GameMode&&); \
	TP_UE5_API ATP_UE5GameMode(const ATP_UE5GameMode&); \
public: \
	TP_UE5_API virtual ~ATP_UE5GameMode();


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TP_UE5_API ATP_UE5GameMode(ATP_UE5GameMode&&); \
	TP_UE5_API ATP_UE5GameMode(const ATP_UE5GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TP_UE5_API, ATP_UE5GameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_UE5GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATP_UE5GameMode) \
	TP_UE5_API virtual ~ATP_UE5GameMode();


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_9_PROLOG
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_SPARSE_DATA \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_RPC_WRAPPERS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_ACCESSORS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_INCLASS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_SPARSE_DATA \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_ACCESSORS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TP_UE5_API UClass* StaticClass<class ATP_UE5GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_TP_UE5GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
