// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TP_UE5/ATriggerable.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeATriggerable() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	TP_UE5_API UClass* Z_Construct_UClass_AATriggerable();
	TP_UE5_API UClass* Z_Construct_UClass_AATriggerable_NoRegister();
	UPackage* Z_Construct_UPackage__Script_TP_UE5();
// End Cross Module References
	void AATriggerable::StaticRegisterNativesAATriggerable()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AATriggerable);
	UClass* Z_Construct_UClass_AATriggerable_NoRegister()
	{
		return AATriggerable::StaticClass();
	}
	struct Z_Construct_UClass_AATriggerable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_collider_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_collider;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_button_MetaData[];
#endif
		static const UECodeGen_Private::FTextPropertyParams NewProp_button;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_verb_MetaData[];
#endif
		static const UECodeGen_Private::FTextPropertyParams NewProp_verb;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AATriggerable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TP_UE5,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AATriggerable_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ATriggerable.h" },
		{ "ModuleRelativePath", "ATriggerable.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AATriggerable_Statics::NewProp_collider_MetaData[] = {
		{ "Category", "ATriggerable" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ATriggerable.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AATriggerable_Statics::NewProp_collider = { "collider", nullptr, (EPropertyFlags)0x001000000008000d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AATriggerable, collider), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AATriggerable_Statics::NewProp_collider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AATriggerable_Statics::NewProp_collider_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AATriggerable_Statics::NewProp_button_MetaData[] = {
		{ "Category", "ATriggerable" },
		{ "ModuleRelativePath", "ATriggerable.h" },
	};
#endif
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UClass_AATriggerable_Statics::NewProp_button = { "button", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AATriggerable, button), METADATA_PARAMS(Z_Construct_UClass_AATriggerable_Statics::NewProp_button_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AATriggerable_Statics::NewProp_button_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AATriggerable_Statics::NewProp_verb_MetaData[] = {
		{ "Category", "ATriggerable" },
		{ "ModuleRelativePath", "ATriggerable.h" },
	};
#endif
	const UECodeGen_Private::FTextPropertyParams Z_Construct_UClass_AATriggerable_Statics::NewProp_verb = { "verb", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AATriggerable, verb), METADATA_PARAMS(Z_Construct_UClass_AATriggerable_Statics::NewProp_verb_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AATriggerable_Statics::NewProp_verb_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AATriggerable_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AATriggerable_Statics::NewProp_collider,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AATriggerable_Statics::NewProp_button,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AATriggerable_Statics::NewProp_verb,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AATriggerable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AATriggerable>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AATriggerable_Statics::ClassParams = {
		&AATriggerable::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AATriggerable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AATriggerable_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AATriggerable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AATriggerable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AATriggerable()
	{
		if (!Z_Registration_Info_UClass_AATriggerable.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AATriggerable.OuterSingleton, Z_Construct_UClass_AATriggerable_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AATriggerable.OuterSingleton;
	}
	template<> TP_UE5_API UClass* StaticClass<AATriggerable>()
	{
		return AATriggerable::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AATriggerable);
	AATriggerable::~AATriggerable() {}
	struct Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_ATriggerable_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_ATriggerable_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AATriggerable, AATriggerable::StaticClass, TEXT("AATriggerable"), &Z_Registration_Info_UClass_AATriggerable, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AATriggerable), 619481815U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_ATriggerable_h_2778011610(TEXT("/Script/TP_UE5"),
		Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_ATriggerable_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_ATriggerable_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
