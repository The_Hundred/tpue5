// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "UTriggerText.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TP_UE5_UTriggerText_generated_h
#error "UTriggerText.generated.h already included, missing '#pragma once' in UTriggerText.h"
#endif
#define TP_UE5_UTriggerText_generated_h

#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_SPARSE_DATA
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetText);


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetText);


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_ACCESSORS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_CALLBACK_WRAPPERS
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUTriggerText(); \
	friend struct Z_Construct_UClass_UUTriggerText_Statics; \
public: \
	DECLARE_CLASS(UUTriggerText, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TP_UE5"), NO_API) \
	DECLARE_SERIALIZER(UUTriggerText)


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUUTriggerText(); \
	friend struct Z_Construct_UClass_UUTriggerText_Statics; \
public: \
	DECLARE_CLASS(UUTriggerText, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TP_UE5"), NO_API) \
	DECLARE_SERIALIZER(UUTriggerText)


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUTriggerText(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUTriggerText) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUTriggerText); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUTriggerText); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUTriggerText(UUTriggerText&&); \
	NO_API UUTriggerText(const UUTriggerText&); \
public: \
	NO_API virtual ~UUTriggerText();


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUTriggerText(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUTriggerText(UUTriggerText&&); \
	NO_API UUTriggerText(const UUTriggerText&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUTriggerText); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUTriggerText); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUTriggerText) \
	NO_API virtual ~UUTriggerText();


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_12_PROLOG
#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_SPARSE_DATA \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_RPC_WRAPPERS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_ACCESSORS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_CALLBACK_WRAPPERS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_INCLASS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_SPARSE_DATA \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_ACCESSORS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_CALLBACK_WRAPPERS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_INCLASS_NO_PURE_DECLS \
	FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TP_UE5_API UClass* StaticClass<class UUTriggerText>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_berge_Desktop_TP_UE5_Source_TP_UE5_UTriggerText_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
