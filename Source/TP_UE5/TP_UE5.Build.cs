// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TP_UE5 : ModuleRules
{
	public TP_UE5(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput" });
	}
}
