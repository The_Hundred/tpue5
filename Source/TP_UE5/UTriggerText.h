// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UTriggerText.generated.h"

/**
 * 
 */
UCLASS()
class TP_UE5_API UUTriggerText : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FText buttonText = FText::FromString("{X}");

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FText verbText = FText::FromString("{press}");;

	UFUNCTION(BlueprintImplementableEvent)
		void UpdateWidgetText();

public:

	UFUNCTION(BlueprintCallable)
		void SetText(FText button, FText verb)
	{
		buttonText = button;
		verbText = verb;

		UpdateWidgetText();
	}

};
};
