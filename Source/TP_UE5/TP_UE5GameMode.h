// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TP_UE5GameMode.generated.h"

UCLASS(minimalapi)
class ATP_UE5GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATP_UE5GameMode();
};



