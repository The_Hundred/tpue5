// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ATriggerable.generated.h"

UCLASS()
class TP_UE5_API AATriggerable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AATriggerable();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBoxComponent* collider = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText button = FText::FromString("{X}");

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText verb = FText::FromString("{press}");

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
