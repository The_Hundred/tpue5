// Copyright Epic Games, Inc. All Rights Reserved.

#include "TP_UE5GameMode.h"
#include "TP_UE5Character.h"
#include "UObject/ConstructorHelpers.h"

ATP_UE5GameMode::ATP_UE5GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
